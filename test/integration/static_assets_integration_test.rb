require 'test_helper'

describe "static assets integration" do
  it "provides bootstrap-editable.js on the asset pipeline" do
    visit '/assets/bootstrap-editable.js'
    # page.text.must_include 'var StaticAsset = {};'
  end

  it "provides bootstrap-editable.css on the asset pipeline" do
    visit '/assets/bootstrap-editable.css'
    # page.text.must_include '.static_asset {'
  end
end
