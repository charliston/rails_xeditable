$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "rails_xeditable/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "rails_xeditable"
  s.version     = RailsXeditable::VERSION
  s.authors     = ["Charliston"]
  s.email       = ["charliston@msn.com"]
  s.homepage    = "https://bitbucket.org/charliston"
  s.summary     = %q{Assets pipeline for x-editable.}
  s.description = %q{Assets pipeline for x-editable for bootstrap3.}
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.1.3"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency 'minitest' # <------- here
  s.add_development_dependency 'capybara' # <------- and here
end
