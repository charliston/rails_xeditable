module StaticAssets
  class Engine < ::Rails::Engine
    initializer 'rails_xeditable.load_static_assets' do |app|
      app.middleware.use ::ActionDispatch::Static, "#{root}/vendor"
    end
  end
end
